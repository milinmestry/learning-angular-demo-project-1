FROM node:lts

RUN mkdir /home/node/tour-of-heroes && chown node:node /home/node/tour-of-heroes
RUN mkdir /home/node/tour-of-heroes/node_modules && chown node:node /home/node/tour-of-heroes/node_modules
WORKDIR  /home/node/tour-of-heroes
USER node
COPY --chown=node:node package.json package-lock.json ./
RUN npm ci --quiet
COPY --chown=node:node . .

# WORKDIR /usr/src/app/angular

# COPY package*.json ./

# RUN npm install -g @angular/cli@latest @angular-devkit/build-angular && npm install npm@latest

# EXPOSE 4201

# CMD ["npm", "start"]