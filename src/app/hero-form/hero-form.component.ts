import { Component } from '@angular/core';
import { HeroClass } from "../heroClass";

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss', './forms.scss']
})

export class HeroFormComponent {
  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];

  model = new HeroClass(18, 'Dr. IQ', this.powers[0], 'Chuck Overstreet');
  submitted = false;
  onSubmit() { this.submitted = true; }

  newHero() {
    this.model = new HeroClass(42, '', '');
  }
}
